from rest_framework.permissions import BasePermission
from django.core.exceptions import ObjectDoesNotExist
from users.models import Employee
from rest_framework.permissions import SAFE_METHODS

class HasRestaurantPermission(BasePermission):
    
    def has_permission(self, request, view):
        return request.method in SAFE_METHODS or request.user.is_authenticated

    def has_object_permission(self, request, view, restaurant):
        try:
            employee = Employee.objects.get(django_user = request.user)
        except ObjectDoesNotExist:
            employee = None
        if employee == None:
            return False
        return employee.restaurant == restaurant