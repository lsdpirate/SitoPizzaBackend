from django.db import models
from djmoney.models.fields import MoneyField

# Create your models here.
class Restaurant(models.Model):
	name = models.CharField(max_length = 30)
	address = models.CharField(max_length = 30)
	street_number = models.CharField(max_length = 10)
	city = models.CharField(max_length = 25)
	region = models.CharField(max_length = 30)
	phone_number = models.CharField(max_length = 15)
	order_minimum = MoneyField(max_digits = 4, decimal_places = 2, default_currency = 'EUR')
	shipping_cost = MoneyField(max_digits = 4, decimal_places = 2, default_currency = 'EUR')

	def __unicode__(self):
		return "{} restaurant in {} {}, {}, {}".format(
			self.name, self.address, self.street_number, self.city, self.region)

	def __str__(self):
		return self.__unicode__()

WEEKDAYS = [
  (1, ("Monday")),
  (2, ("Tuesday")),
  (3, ("Wednesday")),
  (4, ("Thursday")),
  (5, ("Friday")),
  (6, ("Saturday")),
  (7, ("Sunday")),
]

class OpeningHours(models.Model):

    weekday = models.IntegerField(choices=WEEKDAYS)
    from_hour = models.TimeField()
    to_hour = models.TimeField()
    restaurant = models.ForeignKey(Restaurant)

    class Meta:
        ordering = ('weekday', 'from_hour')
        unique_together = (
            ("restaurant", "weekday", "from_hour"),
            ("restaurant", "weekday", "to_hour"),
            ("restaurant", "weekday", "to_hour", "from_hour")
        )

    def __unicode__(self):
        return u'%s: %s - %s' % (self.get_weekday_display(),
                                 self.from_hour, self.to_hour)

    def __str__(self):
    	return self.__unicode__()
