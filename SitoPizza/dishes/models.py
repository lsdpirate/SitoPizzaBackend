from django.db import models
from djmoney.models.fields import MoneyField
from restaurants.models import Restaurant
from orders.models import Order
# Create your models here.

TOPPINGS_MODES = (
	("A", "ADD"),
	("R", "REMOVE")
	)
class Topping(models.Model):
	name = models.CharField(max_length = 20)
	price = MoneyField(max_digits = 5, decimal_places = 2, default_currency = "EUR")
	cathegory = models.CharField(max_length = 20)
	# illustration = models.ImageField()
	restaurant = models.ForeignKey(Restaurant, null = True)

	def save(self, *args, **kwargs):
		self.cathegory = self.cathegory.lower()
		super(Topping, self).save(*args, **kwargs)

	def __unicode__(self):
		return "{} offered by {}".format(self.name, self.restaurant.name)
		
	def __str__(self):
		return self.__unicode__()

class Dish(models.Model):
	name = models.CharField(max_length = 20)
	price = MoneyField(max_digits = 5, decimal_places = 2, default_currency = "EUR")
	menu_cathegory = models.CharField(max_length = 20) # The cathegory which will be shown to users
	cathegory = models.CharField(max_length = 20) # The cathegory for toppings matching
	restaurant = models.ForeignKey(Restaurant)
	toppings = models.ManyToManyField(Topping)

	def __unicode__(self):
		return "{} {}".format(self.name, self.get_full_price())

	def __str__(self):
		return self.__unicode__()

	def get_full_price(self):
		toppings_cost = 0
		for topping in self.toppings.all():
			toppings_cost += topping.price
		return self.price + toppings_cost

	def save(self, *args, **kwargs):
		self.menu_cathegory = self.menu_cathegory.lower()
		self.cathegory = self.cathegory.lower()
		super(Dish, self).save(*args, **kwargs)


class OrderedDish(models.Model):
	original_dish = models.ForeignKey(Dish)
	additional_toppings = models.ManyToManyField(Topping, through = 'ToppingOrderedDishRelationship')
	order = models.ForeignKey(Order, on_delete = models.CASCADE)

	def __unicode__(self):
		return "{} {}".format(self.original_dish.name, self.get_full_price())

	def __str__(self):
		return self.__unicode__()

	def get_full_price(self):
		cost = self.original_dish.get_full_price()
		for rel in ToppingOrderedDishRelationship.objects.filter(ordered_dish = self):
			if rel.mode == "R" and rel.topping.name in [x.name for x in self.original_dish.toppings]:
				cost -= rel.topping.price
			else:
				cost += rel.topping.price
		return cost

class ToppingOrderedDishRelationship(models.Model):
	ordered_dish = models.ForeignKey(OrderedDish)
	topping = models.ForeignKey(Topping)
	mode = models.CharField(max_length = 1, choices = TOPPINGS_MODES, default = "A")
