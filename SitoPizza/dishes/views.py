from django.shortcuts import render, get_object_or_404
from rest_framework.decorators import api_view, permission_classes
from .models import Dish, Topping
from .serializers import DishSerializer, ToppingSerializer, UnmodifiableCathegoryToppingSerializer
from rest_framework.response import Response
from restaurants.models import Restaurant
from rest_framework.exceptions import PermissionDenied, NotAuthenticated
from django.core.exceptions import ObjectDoesNotExist
from users.models import Employee
from restaurants.permissions import HasRestaurantPermission
from rest_framework.views import APIView
# Create your views here.

class DishDetails(APIView):

	permission_classes = (HasRestaurantPermission,)
	
	def get(self, request, restaurant_pk, pk, format = None):
		dish = get_object_or_404(Dish, pk = pk)
		serializer = DishSerializer(instance = dish)
		return Response(serializer.data)

	def put(self, request, restaurant_pk, pk, format = None):
		dish = get_object_or_404(Dish, pk = pk)
		self.check_object_permissions(request, dish.restaurant)
		serializer = DishSerializer(instance = dish, data = request.data, partial = True)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status = 200)
		else:
			return Response(serializer.errors, status = 400)

class RestaurantDishes(APIView):

	permission_classes = (HasRestaurantPermission,)

	def get(self, request, restaurant_pk, format = None):
		"""
		Return the full menu for a restaurant.
		"""
		restaurant = Restaurant.objects.get(pk = int(restaurant_pk))
		dishes = Dish.objects.filter(restaurant = restaurant).order_by("menu_cathegory")
		serializer = DishSerializer(instance = dishes, many = True)
		return Response(serializer.data)

	def post(self, request, restaurant_pk, format = None):
		"""
		Add a new dish to a restaurant.
		"""
		restaurant = Restaurant.objects.get(pk = int(restaurant_pk))
		data = request.data
		data['restaurant'] = restaurant.pk
		serializer = DishSerializer(data = data)
		self.check_object_permissions(request, restaurant)
		if serializer.is_valid():
			serializer.save(restaurant = restaurant)
			return Response(serializer.data, status = 201)
		else:
			return Response(serializer.errors, status = 400)

@api_view(["POST", "GET"])
def restaurant_toppings(request, restaurant_pk):
	
	if request.method == "POST":
		# print(request.user)
		if request.user.is_anonymous:
			raise NotAuthenticated("You have to be logged in to perform this action", code = 401)

		restaurant = get_object_or_404(Restaurant, pk = int(restaurant_pk))
		employee = get_object_or_404(Employee, django_user = request.user)
		topping_serializer = ToppingSerializer(data = request.data)
		if not restaurant == employee.restaurant:
			raise PermissionDenied("You are not authorized in this restaurant")
		if topping_serializer.is_valid():
			topping_serializer.save()
			return Response(topping_serializer.data, status = 201)
		else:
			return Response(topping_serializer.errors, status = 400)
	elif request.method == "GET":
		restaurant = cget_object_or_404(Restaurant, pk = int(restaurant_pk))
		toppings = Topping.objects.filter(restaurant = restaurant)
		serializer = ToppingSerializer(instance = toppings, many = True)
		return Response(serializer.data, status = 200)

class ToppingDetails(APIView):

	permission_classes = (HasRestaurantPermission,)

	def get(self, request, restaurant_pk, topping_pk):
		restaurant = get_object_or_404(Restaurant, pk = int(restaurant_pk))
		topping = get_object_or_404(Topping, restaurant = restaurant, pk = int(topping_pk))
		serializer = ToppingSerializer(instance = topping)
		return Response(serializer.data, status = 200)
		

	def put(self, request, restaurant_pk, topping_pk):
		restaurant = get_object_or_404(Restaurant, pk = int(restaurant_pk))
		employee = get_object_or_404(Employee, django_user = request.user)
		topping = get_object_or_404(Topping, restaurant = restaurant, pk = int(topping_pk))
		self.check_object_permissions(request, restaurant)
		serializer = UnmodifiableCathegoryToppingSerializer(instance = topping, data = request.data, partial = True)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status = 200)
		else:
			return Response(serializer.errors, status = 400)

	def delete(self, request, restaurant_pk, topping_pk):
		restaurant = get_object_or_404(Restaurant, pk = int(restaurant_pk))
		employee = get_object_or_404(Employee, django_user = request.user)
		topping = get_object_or_404(Topping, restaurant = restaurant, pk = int(topping_pk))
		self.check_object_permissions(request, restaurant)
		topping.delete()
		return Response(status = 204)

@api_view(["GET"])
def restaurant_cathegory_toppings(request, restaurant_pk, cathegory):
	
	if request.method == "GET":
		restaurant = get_object_or_404(Restaurant, pk = int(restaurant_pk))
		toppings = Topping.objects.filter(cathegory = cathegory.lower(), restaurant = restaurant)
		serializer = ToppingSerializer(many = True, instance = toppings)
		return Response(serializer.data)

@api_view(["GET"])
def restaurant_menu_cathegory_dishes(request, restaurant_pk, cathegory):
	if request.method == "GET":
		restaurant = Restaurant.objects.get(pk = int(restaurant_pk))
		dishes = Dish.objects.filter(restaurant = restaurant, menu_cathegory = cathegory)
		serializer = DishSerializer(instance = dishes, many = True)
		return Response(serializer.data)