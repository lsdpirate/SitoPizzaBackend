from rest_framework import serializers
from .models import Topping, Dish, OrderedDish, TOPPINGS_MODES
from .models import ToppingOrderedDishRelationship
from SitoPizza.serializers import MoneySerializer
from orders.models import Order
from restaurants.models import Restaurant
from django.shortcuts import get_object_or_404


class ToppingSerializer(serializers.ModelSerializer):
    price = MoneySerializer()
    id = serializers.IntegerField(source = 'pk', read_only = True)

    def to_internal_value(self, data):
        if 'id' in data:
            return Topping.objects.get(pk = data['id'])
        else:
            return super(ToppingSerializer, self).to_internal_value(data)

    class Meta:
        model = Topping
        fields = ("id", "name", "price", "cathegory")

class BriefToppingSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(source = "pk", read_only = True)
    
    def to_internal_value(self, data):
        if 'id' in data:
            return Topping.objects.get(pk = data['id'])
        else:
            return super(BriefToppingSerializer, self).to_internal_value(data)
    
    class Meta:
        model = Topping
        fields = ("id", "name")

class UnmodifiableCathegoryToppingSerializer(ToppingSerializer):
    """
    This serializer won't allow the cathegory to be changed.
    This is necessary since changing the cathegory would break
    consistency expecially in ordered dishes.
    """
    cathegory = serializers.CharField(read_only = True)

    class Meta:
        model = Topping
        fields = ("id", "name", "price", "cathegory")

class DishSerializer(serializers.ModelSerializer):
    toppings = BriefToppingSerializer(many = True)
    price = MoneySerializer()
    toppings_cathegory = serializers.CharField(source = "cathegory")
    id = serializers.IntegerField(source = 'pk', read_only = True)
    restaurant = serializers.PrimaryKeyRelatedField(write_only= True, queryset = Restaurant.objects)

    def validate(self, data):
        """
        Custom validation method to check whether the toppings and the
        dish are served by the same restaurant.
        """
        data = super(DishSerializer, self).validate(data)
        if not "toppings" in data:
            return data
        for topping in data["toppings"]:
            if not topping.cathegory == data["cathegory"]:
                raise serializers.ValidationError("Invalid topping {} for this dish's cathegory {}"
                    .format(topping, data["cathegory"]))
            elif not topping.restaurant == data["restaurant"]:
                raise serializers.ValidationError("This topping {} is not served in this restaurant {}"
                    .format(topping, data["restaurant"]))
                
        return data

    def create(self, data):
        """
        Custom creation method needed because of the M2M
        relationship with toppings.
        """
        dish = Dish.objects.create(name = data['name'],
            price = data['price'],
            menu_cathegory = data['menu_cathegory'],
            cathegory = data['cathegory'],
            restaurant = data['restaurant']
        )
        for topping in data['toppings']:
            dish.toppings.add(topping)
        return dish

    class Meta:
        model = Dish
        fields = ("id", "name", "price", "menu_cathegory", "toppings_cathegory", "toppings", "restaurant")

class ToppingOrderedDishRelationshipSerializer(serializers.ModelSerializer):
    """
    A serializer for the topping-ordered_dish relationship.
    """

    topping = ToppingSerializer()
    class Meta:
        model = ToppingOrderedDishRelationship
        fields = ("topping", "mode")

class OrderedDishSerializer(serializers.ModelSerializer):
    original_dish = DishSerializer(many = False, read_only = True)
    additional_toppings = ToppingOrderedDishRelationshipSerializer(
        many = True, read_only = True, source = "toppingordereddishrelationship_set")

    class Meta:
        model = OrderedDish
        fields = ("original_dish", "additional_toppings")


class OrderIssueDishSerializer(serializers.ModelSerializer):
    """
    OrderedDish DE-serializer, could be merged into the original 
    OrderedDish serializer when a working logic that will make it
    possible for serialization and de-serialization to work together
    is figured out.

    WHY: When deserializing an order (most frequently from data of an order
    issued by the user) special attention has to be taken for the creation
    of the OrderedDish object. Since this object needs an order to point to
    we cannot create it until the order is finalized.
    """
    additional_toppings = ToppingOrderedDishRelationshipSerializer(
        many = True, read_only = True, source = "toppingordereddishrelationship_set")
    original_dish = serializers.PrimaryKeyRelatedField(many = False, queryset = Dish.objects)
    order = serializers.PrimaryKeyRelatedField(many = False, queryset = Order.objects, allow_null = True)

    def to_internal_value(self, data):
        """
        It was necessary to override the to_internal_value method
        since the default one would try to create an OrderedDish object
        which cannot exist without an Order to reference.
        At the time this method gets called there is no Order object yet 
        since the OrderIssue serializer's create method has not been
        called yet.
        """
        return data

    def validate(self, data):
        restaurant = data['restaurant']
        original_dish = get_object_or_404(Dish, pk = data['original_dish'])

        if not original_dish.restaurant == restaurant:
            raise serializers.ValidationError("The selected dish is not server by this restaurant"
                .format(data["original_dish"]))
        for topping in data['additional_toppings']:
            try:
                topping = Topping.objects.get(pk = topping['pk'])
            except Topping.DoesNotExist:
                raise serializers.ValidationError("The topping with id {} does not exist"
                    .format(topping['pk']))
            if not topping.restaurant == restaurant:
                raise serializers.ValidationError("{} topping is not served by this restaurant"
                    .format(topping))
        ret = data.copy()
        del ret['restaurant']
        return ret



    def create(self, validated_data):
        """
        Create the Python object from the data in this serializer
        and save it to database.
        """
        original_dish = Dish.objects.get(pk = validated_data['original_dish'])
        ordered_dish = OrderedDish.objects.create(original_dish = original_dish,
            order = validated_data['order'])
        ordered_dish.save()
        for topping in validated_data['additional_toppings']:
            relationship = ToppingOrderedDishRelationship.objects.create(
                ordered_dish = ordered_dish,
                topping = Topping.objects.get(pk = topping['pk']),
                mode = topping['mode'])
            relationship.save()
        return ordered_dish

    class Meta:
        model = OrderedDish
        fields = ("original_dish", "additional_toppings", "order")