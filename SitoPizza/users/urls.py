from django.conf.urls import url
from .views import user_view, user_details, user_login

urlpatterns = [
    url(r"^$", user_view),
    url(r"^(?P<user_pk>[0-9]+)/?$", user_details),
    url(r"^login/?$", user_login)
]