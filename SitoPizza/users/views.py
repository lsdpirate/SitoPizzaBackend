from django.shortcuts import render, get_object_or_404
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from users.models import User
from users.serializers import UserSerializer
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth import authenticate, login
from django.template import loader
from django.http import HttpResponse

@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def user_view(request):
    if request.method == "GET":
        users = User.objects.all()
        serializer = UserSerializer(users, many = True)
        return Response(serializer.data)

@api_view(["GET"])
@permission_classes((IsAuthenticated,))
def user_details(request, user_pk):
    if request.method == "GET":
        user = get_object_or_404(User, pk = int(user_pk))
        if not user.django_user == request.user:
            raise PermissionDenied(detail = "You cannot see this user's details")
        serializer = UserSerializer(instance = user)
        return Response(serializer.data)

def user_login(request):
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username = username, password = password)
        if user is not None:
            login(request, user)
            return HttpResponse(status = 200)
        else:
            return HttpResponse(status = 401)
    elif request.method == "GET":
        return render(request, "login.html")