from django.db import models
from django.conf import settings
from restaurants.models import Restaurant
# Create your models here.

class User(models.Model):
	django_user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
	address = models.CharField(max_length = 30)
	street_number = models.CharField(max_length = 10)
	city = models.CharField(max_length = 25)
	region = models.CharField(max_length = 30)
	phone_number = models.CharField(max_length = 15)

	def __str__(self):
		djangouser = self.django_user
		return djangouser.username


class Employee(User):
	restaurant = models.ForeignKey(Restaurant)