from django.contrib import admin
from .models import User, Employee

# Register your models here.

class UserAdmin(admin.ModelAdmin):
	pass

class EmployeeAdmin(admin.ModelAdmin):
	pass

admin.site.register(User, UserAdmin)
admin.site.register(Employee, EmployeeAdmin)
