from django.db import models
from djmoney.models.fields import MoneyField
from users.models import User
from restaurants.models import Restaurant
# Create your models here.

ORDER_STATUS = (
		("P", "Pending"),
		("A", "Approved"),
		("R", "Rejected")
	)

class Order(models.Model):
	date = models.DateTimeField(auto_now_add = True)
	total_price = MoneyField(max_digits = 6, decimal_places = 2, default_currency = 'EUR')
	status = models.CharField(choices = ORDER_STATUS, max_length = 10)
	user = models.ForeignKey(User)
	restaurant = models.ForeignKey(Restaurant)
	address = models.CharField(max_length = 30)
	street_number = models.CharField(max_length = 10)
	city = models.CharField(max_length = 25)
	region = models.CharField(max_length = 30)
	phone_number = models.CharField(max_length = 15)


	def __str__(self):
		username = self.user.django_user.username
		return "{}'s order issued the {}".format(username, self.date)

	def save(self, *args, **kwargs):
		price = 0
		for dish in self.ordereddish_set.all():
			price += dish.original_dish.price
			for topping in dish.additional_toppings.all():
				price += topping.price
		self.total_price = price
		super(Order, self).save(*args, **kwargs)