from rest_framework.permissions import BasePermission, SAFE_METHODS
from users.models import User, Employee

class HasOrderStatusPermission(BasePermission):

    def has_permission(self, request, view):
        if not (request.user.is_authenticated) or request.user.is_anonymous:
            raise permissions.NotAuthenticated()
        else:
            return True

    def has_object_permission(self, request, view, obj):
        """
        An order is read only to its user and its status can be modified
        by the restaurant's employees only.
        """

        if request.method == "GET":
            try:
                user = User.objects.get(django_user = request.user)
            except User.DoesNotExist:
                return False
            if user == obj.user:
                return True
            try:
                employee = Employee.objects.get(django_user = request.user)
            except Employee.DoesNotExist:
                return False
            return employee.restaurant == obj.restaurant

        elif request.method == "PUT":
            user = None
            try:
                user = Employee.objects.get(django_user = request.user)
            except Employee.DoesNotExist:
                return False
            return user.restaurant == obj.restaurant

class HasOrderViewPermission(BasePermission):

    def has_permission(self, request, view):
        authenticated = request.user.is_authenticated
        try:
            user = User.objects.get(django_user = request.user)
        except User.DoesNotExist:
            user = None
        return user and (not (user == None)) and authenticated

    def has_object_permission(self, request, view, obj):
        try:
            user = User.objects.get(django_user = request.user)
        except User.DoesNotExist:
            user = None

        return user and user == obj.user
