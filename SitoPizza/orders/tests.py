from django.test import TestCase
from rest_framework.test import APITestCase
from django.contrib.auth.models import User
import json
# Create your tests here.

class OrderIssueTest(APITestCase):
    fixtures = ['dump.json',]

    def test_order_issue(self):
        url = "/orders/"
        data = json.dumps(
            {
            "restaurant": 1,
            "ordered_dishes": [
                {
                    "original_dish": 1,
                    "additional_toppings": [
                        {
                            "pk" : 1,
                            "mode" : "add"
                        }
                    ]
                }
            ],
            "address" : "Via roma",
            "street_number" : "3",
            "phone_number" : "333333333",
            "city" : "Roma",
            "region" : "Lazio"
            }
        )
        user = User.objects.get(username = 'lsdpirate')
        self.client.force_authenticate(user)
        response = self.client.post(url, data, content_type = 'application/json')
        self.assertEqual(response.status_code, 201)
        self.client.logout()

    def test_order_issue_non_existend_dish(self):
        url = "/orders/"
        data = json.dumps(
            {
            "restaurant": 1,
            "ordered_dishes": [
                {
                    "original_dish": -1,
                    "additional_toppings": [
                        {
                            "pk" : 1,
                            "mode" : "add"
                        }
                    ]
                }
            ],
            "address" : "Via roma",
            "street_number" : "3",
            "phone_number" : "333333333",
            "city" : "Roma",
            "region" : "Lazio"
            }
        )
        user = User.objects.get(username = 'lsdpirate')
        self.client.force_authenticate(user)
        response = self.client.post(url, data, content_type = 'application/json')
        self.assertEqual(response.status_code, 404)
        self.client.logout()

    def test_order_issue_dish_not_served(self):
        url = "/orders/"
        data = json.dumps(
            {
            "restaurant": 1,
            "ordered_dishes": [
                {
                    "original_dish": 3,
                    "additional_toppings": [
                        {
                            "pk" : 1,
                            "mode" : "add"
                        }
                    ]
                }
            ],
            "address" : "Via roma",
            "street_number" : "3",
            "phone_number" : "333333333",
            "city" : "Roma",
            "region" : "Lazio"
            }
        )
        user = User.objects.get(username = 'lsdpirate')
        self.client.force_authenticate(user)
        response = self.client.post(url, data, content_type = 'application/json')
        self.assertEqual(response.status_code, 400)
        self.client.logout()

    def test_order_issue_no_login(self):
        url = "/orders/"
        response = self.client.post(url, "", content_type = 'application/json')
        self.assertEqual(response.status_code, 403)

class OrderSerializerTest(APITestCase):
    fixtures = ["dump",]

    def test_order_serialization(self):
        url = "/orders/1/"
        user = User.objects.get(username = 'lsdpirate')
        self.client.force_authenticate(user)
        response = self.client.get(url)
        expected_response = {
            "order_id": 1,
            "date": "2017-07-23T11:19:40.797000Z",
            "total_price": {
                "currency": "EUR",
                "amount": "4.00"
            },
            "status": "A",
            "restaurant_name": "Apollo",
            "ordered_dishes": [
                {
                    "original_dish": {
                        "pk": 1,
                        "name": "Pizza salame",
                        "price": {
                            "currency": "EUR",
                            "amount": "4.00"
                        },
                        "menu_cathegory": "pizza",
                        "toppings": [
                            {
                                "name": "Salame",
                                "price": {
                                    "currency": "EUR",
                                    "amount": "1.00"
                                },
                                "cathegory": "pizza"
                            }
                        ]
                    },
                    "additional_toppings": []
                },
                {
                    "original_dish": {
                        "pk": 1,
                        "name": "Pizza salame",
                        "price": {
                            "currency": "EUR",
                            "amount": "4.00"
                        },
                        "menu_cathegory": "pizza",
                        "toppings": [
                            {
                                "name": "Salame",
                                "price": {
                                    "currency": "EUR",
                                    "amount": "1.00"
                                },
                                "cathegory": "pizza"
                            }
                        ]
                    },
                    "additional_toppings": [
                        {
                            "topping": {
                                "name": "Salame",
                                "price": {
                                    "currency": "EUR",
                                    "amount": "1.00"
                                },
                                "cathegory": "pizza"
                            },
                            "mode": "A"
                        }
                    ]
                }
            ]
        }
        self.assertEqual(response.json(), expected_response)


class OrderPermissionsTest(APITestCase):
    fixtures = ["dump"]
    
    def test_cross_user_access(self):
        url = "/orders/1/"
        user = User.objects.get(username = 'mario')
        self.client.force_authenticate(user)
        response = self.client.get(url)
        self.assertEquals(response.status_code, 403)

    def test_change_order_status_allowed(self):
        url = "/orders/1/"
        user = User.objects.get(username = 'aziz')
        data = {"status" : "A"}
        self.client.force_authenticate(user)
        response = self.client.put(url, data = json.dumps(data), content_type = "application/json")
        self.assertEquals(response.status_code, 200)

    def test_change_order_status_not_allowed(self):
        url = "/orders/1/"
        user = User.objects.get(username = 'ling')
        data = {"status" : "A"}
        self.client.force_authenticate(user)
        response = self.client.put(url, data = json.dumps(data), content_type = "application/json")
        self.assertEquals(response.status_code, 403)

    def test_change_order_status_invalid_request(self):
        url = "/orders/1/"
        user = User.objects.get(username = 'aziz')
        data = {"error" : 1}
        self.client.force_authenticate(user)
        response = self.client.put(url, data = json.dumps(data), content_type = "application/json")
        self.assertEquals(response.status_code, 400)        