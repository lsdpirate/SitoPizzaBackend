from django.conf.urls import url
from .views import OrderDetails, UserOrders

urlpatterns = [
    url(r"(?P<pk>[0-9]+)/?$", OrderDetails.as_view()),
    url(r"^$", UserOrders.as_view())
]