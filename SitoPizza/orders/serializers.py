from rest_framework import serializers
from rest_framework.serializers import StringRelatedField
from .models import Order, ORDER_STATUS
from restaurants.serializers import RestaurantSerializer
from SitoPizza.serializers import MoneySerializer
from dishes.serializers import OrderedDishSerializer, OrderIssueDishSerializer
from SitoPizza.serializers import MoneySerializer
from djmoney.models.fields import MoneyField
from users.models import User
from restaurants.models import Restaurant
from collections import OrderedDict

class OrderSerializer(serializers.ModelSerializer):
    restaurant_name = StringRelatedField(source="restaurant.name", many = False)
    total_price = MoneySerializer()    
    status = serializers.ChoiceField(choices =  ORDER_STATUS, allow_blank = True)
    order_id = serializers.IntegerField(source = "pk")

    class Meta:
        model = Order
        fields = ("order_id", "date", "total_price", "status", "restaurant_name")

class EmployeeModifiableOrderSerializer(serializers.ModelSerializer):
    restaurant_name = StringRelatedField(source="restaurant.name", many = False, read_only = True)
    total_price = MoneySerializer(read_only = True, required = False)
    status = serializers.ChoiceField(choices = ORDER_STATUS, read_only = False)
    date = serializers.DateTimeField(read_only = True)

    class Meta:
        model = Order
        fields = ("date", "total_price", "status", "restaurant_name")

class OrderDetailedSerializer(serializers.ModelSerializer):
    restaurant_name = StringRelatedField(source="restaurant.name", many = False)
    total_price = MoneySerializer()
    status = serializers.ChoiceField(choices = ORDER_STATUS)
    ordered_dishes = OrderedDishSerializer(many = True, source = "ordereddish_set")
    date = serializers.DateTimeField()
    order_id = serializers.IntegerField(source = "pk")
    
    class Meta:
        model = Order
        fields = ("order_id", "date", "total_price", "status", "restaurant_name", "ordered_dishes")


class OrderIssueSerializer(serializers.ModelSerializer):
    
    ordered_dishes = OrderIssueDishSerializer(many = True, source = "ordereddish_set")
    restaurant = serializers.PrimaryKeyRelatedField(many = False, queryset = Restaurant.objects)
    user = serializers.PrimaryKeyRelatedField(many = False, queryset = User.objects)
    address = serializers.CharField(max_length = 30)
    street_number = serializers.CharField(max_length = 6)
    phone_number = serializers.CharField(max_length = 15)
    region = serializers.CharField(max_length = 25)
    city = serializers.CharField(max_length = 25)

    def create(self, validated_data):
        order = Order.objects.create(user = validated_data['user'],
            restaurant = validated_data['restaurant'], total_price = 0,
            address = validated_data['address'], street_number = validated_data['street_number'],
            region = validated_data['region'], phone_number = validated_data['phone_number'],
            city = validated_data['city'])
        order.save()
        for dish in validated_data['ordered_dishes']:
            dish['restaurant'] = validated_data['restaurant']
            dish['order'] = order
            od_serializer = OrderIssueDishSerializer(data = dish)
            od_serializer.is_valid(raise_exception = True)
            od_serializer.save()
        return order

    def to_internal_value(self, data):
        restaurant = Restaurant.objects.get(pk = data['restaurant'])
        user = data['user']
        dishes = []
        for dish in data['ordered_dishes']:
            dish['restaurant'] = restaurant
            dish['order'] = None
            serializer = OrderIssueDishSerializer(data = dish)
            serializer.is_valid(raise_exception = True)
            dishes.append(serializer.validated_data)
        data['ordered_dishes'] = dishes
        data['restaurant'] = restaurant
        data['user'] = user
        return data

    def validate(self, data):
        for field in self.fields:
            if not field in data.keys():
                raise serializers.ValidationError("No value for {} was provided".format(field))
        restaurant = data['restaurant']
        user = data['user']
        for dish in data['ordered_dishes']:
            dish['restaurant'] = restaurant
            dish['order'] = None
            serializer = OrderIssueDishSerializer(data = dish, partial = True)
            serializer.is_valid(raise_exception = True)
            dish = serializer.validated_data
        return data

    class Meta:
        model = Order
        fields = ("restaurant", "ordered_dishes", "user", "address", "phone_number", "street_number", "region", "city")