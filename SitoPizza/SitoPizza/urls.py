"""SitoPizza URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework.urlpatterns import format_suffix_patterns
import users.urls as users_urls
import restaurants.urls as restaurants_urls
import orders.urls as orders_urls
import dishes.urls as dishes_urls

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r"^users/", include(users_urls)),
    url(r"^restaurants/", include(restaurants_urls)),
    url(r"^orders/", include(orders_urls)),
    url(r"^dishes/", include(dishes_urls))
]

urlpatterns = format_suffix_patterns(urlpatterns)
